
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/headerUser.jspf"%>
	<%-- HEADER --%>

	<div class="wrap">
		<div class="image-slider">
			<!-- Slideshow 1 -->
			<ul class="rslides" id="slider1">
				<li><img src="${app}/web/images/slide1.jpg" alt=""></li>
				<li><img src="${app}/web/images/slide2.jpg" alt=""></li>
				<li><img src="${app}/web/images/slide1.jpg" alt=""></li>
			</ul>
			<!-- Slideshow 2 -->
		</div>
		<!--End-image-slider---->
		<!---start-content---->
		<div class="content">
		
		</div>
		<!---End-content---->
		<div class="clear"></div>
	</div>

	<div class="clear"></div>

	<!---end-wrap---->
<%@ include file="/WEB-INF/jspf/login.jspf"%>

<%@ include file="/WEB-INF/jspf/footer.jspf"%>

</body>

</html>
