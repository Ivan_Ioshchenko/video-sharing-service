<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header_reg.jspf"%>
	<%-- HEADER --%>
	



	<div class="wrap">
		<!---start-content---->

		<div class="content">
			<div class="contact">

				<div class="span_2_of_3">
					<div class="reg-form">

						<div class="contact-form">
							<div class="reg-text">
								<h2>Pass recovery</h2>

								<form name="form1" action="passwordRecovery" method="post">

									<div class="contact-form2">
										<span><label>Please enter your E-mail:</label></span>
										<div class="error">
											<c:out value="${errorMessage}" />
										</div>

									</div>
									<div>
										<span><input name="email" type="text" class="text"
											maxlength="40"></span>
									</div>
									<div class="contact-form2">

										<br> <br> <span><input type="submit"
											value="Recover pass"></span>

									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
