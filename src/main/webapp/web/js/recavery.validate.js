
$(function() {
	form = $('form');

	$('form').submit(function() {
		form.find('.error2').remove();

		var valid = 0;	
		var fildUserEmail = form.find('input[name=email]');
		

		if (validEmailFild(fildUserEmail)) {
			valid++;
		}

		if (valid < 1) {
			return false;
		}
		
		return true;

	});

});



function validEmailFild(fild) {
	fild.css({
		'border' : 'none'
	});
	var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	if (pattern.test(fild.val())) {
		return true;
	} else {
		errorMessage(fild, 'Input correct e-mail.');
		return false;
	}
}

function errorMessage(fild, mes) {
	fild.before('<div class="error2">' + mes + '</div>');
	fild.css({
		'border' : 'red 1px solid'
	});

}