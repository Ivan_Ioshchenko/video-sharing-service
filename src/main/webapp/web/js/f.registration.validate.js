$(document)
		.ready(
				function() {

					$("#regform")
							.validate(
									{

										rules : {

											userName : {
												required : true,
												minlength : 4,
												maxlength : 20,
											},

											userEmail : {
												required : true,
												email : true
											},

											pswd : {
												required : true,
												minlength : 6
											},

											cpswd : {
												minlength : 6,
												equalTo : "#pswd"
											}
										},

										messages : {

											userName : {
												required : "Это поле обязательно для заполнения",
												minlength : "Логин должен быть минимум 4 символа",
												maxlength : "Максимальное число символо - 16",
											},

											pswd : {
												minlength : "Пароль должен быть минимум 6 символа",
												maxlength : 16,
											},

										}

									});

				});
