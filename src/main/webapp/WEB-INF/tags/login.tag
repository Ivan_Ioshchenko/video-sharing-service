<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="app" value="${pageContext.request.contextPath}" />
<c:if test="${empty user}">
	<div class="header-top-nav">
		<ul>
			<li><a href="registration.jsp"> Registration </a></li>
			<label> </label>
			<li class="last"><a href="login.jsp">
					Log in </a></li>
		</ul>
	</div>

</c:if>

<c:if test="${not empty user}">
	<div class="header-top-nav">
		<ul>
			<li><a href=""> <c:out
						value="${user.firstName} ${user.lastName}"></c:out>
			</a></li>
			<label> </label>
			<li class="last"><a href="${app}/controller?command=logout"> Log out </a></li>
		</ul>
	</div>

</c:if>

