
<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="app" value="${pageContext.request.contextPath}" />

<c:if test="${user.roleId=='0' }">

	<div class="left-nav">
			<ul>
				<li class="active"><a href="webRoom.jsp">Subscriptions</a></li>
				<li><a href="#">Lecture</a></li>
				<li><a href="#">History</a></li>
				<li><a href="#">Message</a></li>							
			</ul>
	</div>

</c:if>

<c:if test="${user.roleId=='1' }">

	<div class="left-nav">
			<ul>
				<li class="active"><a href="webRoom.jsp">Subscriptions</a></li>
				<li><a href="#">Lecture</a></li>
				<li><a href="#">History</a></li>
				<li><a href="#">Message</a></li>
				<li><a href="#">My Lecture</a></li>							
			</ul>
	</div>

</c:if>
<c:if test="${user.roleId=='2' }">

	<div class="left-nav">
			<ul>
				<li class="active"><a href="webRoom.jsp">Last event</a></li>
				<li><a href="#">Lecture</a></li>
				<li><a href="#">User list</a></li>
				<li><a href="#">Message</a></li>							
			</ul>
	</div>

</c:if>