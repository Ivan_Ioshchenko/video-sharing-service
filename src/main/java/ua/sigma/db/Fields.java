package ua.sigma.db;

public class Fields {
	
	public static final String USER_FIRST_NAME = "first_name";
	public static final String USER_LAST_NAME = "last_name";
	public static final String USER_PASSWORD = "password";
	public static final String USER_EMAIL = "email";
	public static final String USER_ROLE_ID = "role_id";
	public static final String USER_STATUS = "status_id";

}
