package ua.sigma.db.DAO;

import ua.sigma.entity.User;

public interface UserDAO {
	public User get(String email);

	public void add(User user);

	public void update(String login, String password);

}
