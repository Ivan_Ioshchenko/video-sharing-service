package ua.sigma.db.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.sigma.db.DBManager;
import ua.sigma.db.Fields;
import ua.sigma.db.SQL;
import ua.sigma.db.DAO.UserDAO;
import ua.sigma.entity.User;

public class UserDAOImpl implements UserDAO {
	private Logger logger = Logger.getLogger(UserDAO.class);

	public UserDAOImpl() {
	}

	@Override
	public User get(String email) {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection connection = null;

		try {
			DBManager manager = DBManager.getInstance();
			connection = manager.getConnection();

			pstmt = connection.prepareStatement(SQL.FIND_USER_BY_EMAIL);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
			rs.close();
			pstmt.close();

		} catch (SQLException | ClassNotFoundException ex) {
			logger.error("Not get user. -->> ", ex);
			rollbackAndClose(connection);
		}

		return user;
	}

	@Override
	public void add(User user) {
		PreparedStatement pstmt = null;
		Connection connection = null;

		try {
			DBManager manager = DBManager.getInstance();
			connection = manager.getConnection();

			pstmt = connection.prepareStatement(SQL.INSERT_USER);
			pstmt.setString(1, user.getFirstName());
			pstmt.setString(2, user.getLastName());
			pstmt.setString(3, user.getEmail());
			pstmt.setString(4, user.getPassword());
			pstmt.setInt(5, user.getRoleId());
			pstmt.setInt(6, user.getStatus());
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException | ClassNotFoundException ex) {
			logger.error("Not add user. -->> ", ex);
			rollbackAndClose(connection);
		} finally {
			commitAndClose(connection);
		}

	}

	@Override
	public void update(String email, String password) {

		PreparedStatement pstmt = null;
		Connection connection = null;
		try {
			DBManager manager = DBManager.getInstance();
			connection = manager.getConnection();
			pstmt = connection.prepareStatement(SQL.UPDATE_USER);
			pstmt.setString(1, password);
			pstmt.setString(2, email);

			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException | ClassNotFoundException ex) {
			logger.error("Not update user.", ex);
			rollbackAndClose(connection);
		} finally {
			commitAndClose(connection);
		}

	}

	private User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
		user.setLastName(rs.getString(Fields.USER_LAST_NAME));
		user.setPassword(rs.getString(Fields.USER_PASSWORD));
		user.setEmail(rs.getString(Fields.USER_EMAIL));
		user.setRoleId(rs.getInt(Fields.USER_ROLE_ID));
		user.setStatus(rs.getInt(Fields.USER_STATUS));

		return user;
	}

	private void rollbackAndClose(Connection connection) {
		try {
			connection.rollback();
			connection.close();
		} catch (SQLException e) {
		}
	}

	private void commitAndClose(Connection connection) {
		try {
			connection.commit();
			connection.close();
		} catch (SQLException e) {
		}
	}

}
