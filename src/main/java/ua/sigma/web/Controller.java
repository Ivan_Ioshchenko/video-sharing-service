package ua.sigma.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.sigma.web.command.Command;
import ua.sigma.web.command.CommandContainer;

public class Controller extends HttpServlet {

	private static final long serialVersionUID = 3099721537331697043L;
	private Logger logger = Logger.getLogger(Controller.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String commandName = request.getParameter("command");
		
		logger.debug("Command -->>"+commandName);
		Command command = CommandContainer.get(commandName);
		String forward = command.execute(request, response);

		if (forward.endsWith(".jsp")) {
			response.sendRedirect(forward);
		} else {
			RequestDispatcher disp = request.getRequestDispatcher(forward);
			disp.forward(request, response);
		}

	}

}
