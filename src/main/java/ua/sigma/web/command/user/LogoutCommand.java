package ua.sigma.web.command.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.sigma.web.Path;
import ua.sigma.web.command.Command;

public class LogoutCommand extends Command {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		if (session != null) {
			session.invalidate();
		}
		return Path.PAGE_INDEX;
	}

}
