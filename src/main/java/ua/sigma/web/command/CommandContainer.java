package ua.sigma.web.command;

import java.util.HashMap;
import java.util.Map;


import ua.sigma.web.command.user.LogoutCommand;
import ua.sigma.web.command.user.NoCommand;

public class CommandContainer {

	private static Map<String, Command> commands = new HashMap<String, Command>();

	static {
		commands.put("noCommand", new NoCommand());
		commands.put("logout", new LogoutCommand());

	}

	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			return commands.get("noCommand");
		}
		return commands.get(commandName);
	}

}
