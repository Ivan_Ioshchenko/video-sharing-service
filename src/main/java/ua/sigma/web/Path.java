package ua.sigma.web;

public class Path {
	public static final String PAGE_INDEX = "index.jsp";
	public static final String PAGE_REGISTRATION = "registration.jsp";
	public static final String PAGE_LOGIN = "login.jsp";
	public static final String PAGE_WEBROOM = "webRoom.jsp";

	public static final String PAGE_MESSAGE = "message.jsp";
	public static final String PAGE_MESSAGE_RECOVERY = "messageRecovery.jsp";
	public static final String PAGE_RECOVERY = "recovery.jsp";

}
