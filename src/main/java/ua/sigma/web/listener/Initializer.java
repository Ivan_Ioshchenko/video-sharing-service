package ua.sigma.web.listener;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ua.sigma.db.DBManager;
import ua.sigma.service.UserService;

public class Initializer implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		initCommandContainer();
		ServletContext context = contextEvent.getServletContext();

		context.setAttribute("userService", new UserService());

	}

	private void initCommandContainer() {
		try {
			Class.forName("ua.sigma.web.command.CommandContainer");
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException(ex);
		}
	}

}
