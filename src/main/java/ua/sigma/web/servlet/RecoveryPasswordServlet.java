package ua.sigma.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.sigma.entity.User;
import ua.sigma.service.MailService;
import ua.sigma.service.UserService;
import ua.sigma.service.mail.MailMessages;
import ua.sigma.service.mail.MailSubject;
import ua.sigma.web.Path;
import ua.sigma.web.SystemMessage;

public class RecoveryPasswordServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(Path.PAGE_RECOVERY).forward(request,
				response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("email");

		UserService userService = (UserService) request.getServletContext()
				.getAttribute("userService");
		if (userService.exist(email)) {
			User user = userService.get(email);

			if (user.getStatus() == 0) {
				request.setAttribute("messageSubject",
						SystemMessage.SUBJECT_WAIT);
				request.setAttribute("messageText", SystemMessage.MESSAGE_WAIT);
				request.getRequestDispatcher(Path.PAGE_MESSAGE).forward(
						request, response);
			}

			if (user.getStatus() == 1) {

				String[] recipient = new String[] { email };

				String password = genPassword(8);
				new MailService().sendConfirmMessage(recipient,
						MailSubject.RECOVERY_PASSWORD,
						(MailMessages.RECOVERY_PASSWORD + password));
				
				userService.recoveryPassword(email, password);

				request.setAttribute("messageSubject",
						SystemMessage.SUBJECT_PASWORD_RECOVERY);
				request.setAttribute("messageText",
						SystemMessage.MESSAGE_PASWORD_RECOVERY);
				request.setAttribute("recoveryText",
						SystemMessage.MESSAGE_PASWORD_RECOVERY2);
				request.getRequestDispatcher(Path.PAGE_MESSAGE_RECOVERY).forward(
						request, response);
				

			}

			if (user.getStatus() == 2) {
				request.setAttribute("messageSubject",
						SystemMessage.SUBJECT_DENIED);
				request.setAttribute("messageText",
						SystemMessage.MESSAGE_DENIED);
				request.getRequestDispatcher(Path.PAGE_MESSAGE).forward(
						request, response);
			}

		} else {
			request.setAttribute("email", email);
			request.setAttribute("errorMessage", "This email not exist!");
			doGet(request, response);

		}
	}

	private String genPassword(int lenght) {
		StringBuffer buffer = new StringBuffer();
		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for (int i = 0; i < lenght; i++) {
			double index = Math.random() * characters.length();
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}
}
