package ua.sigma.web.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.sigma.entity.User;
import ua.sigma.service.UserService;
import ua.sigma.web.Path;
import ua.sigma.web.SystemMessage;

public class LoginServlet extends HttpServlet {

	private Logger logger = Logger.getLogger(LoginServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(Path.PAGE_LOGIN)
				.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("email");
		String password = request.getParameter("pswd");

		UserService userService = (UserService) request.getServletContext()
				.getAttribute("userService");

		Map<String, String> errorMap = new HashMap<String, String>();

		if (userService.exist(email)) {
			User user = userService.get(email);
			if (user.getPassword().equals(password)) {

				if (user.getStatus() == 0) {

					request.setAttribute("messageSubject",
							SystemMessage.SUBJECT_WAIT);
					request.setAttribute("messageText",
							SystemMessage.MESSAGE_WAIT);
					request.getRequestDispatcher(Path.PAGE_MESSAGE).forward(
							request, response);

				}
				if (user.getStatus() == 1) {
					HttpSession session = request.getSession();
					session.setAttribute("user", user);

					response.sendRedirect(Path.PAGE_WEBROOM);
				}
				if (user.getStatus() == 2) {
					request.setAttribute("messageSubject",
							SystemMessage.SUBJECT_DENIED);
					request.setAttribute("messageText",
							SystemMessage.MESSAGE_DENIED);
					request.getRequestDispatcher(Path.PAGE_MESSAGE).forward(
							request, response);
				}

			} else {
				errorMap.put("email",
						"User with this login password not exist!");
				request.setAttribute("email", email);
				request.setAttribute("errorMap", errorMap);
				doGet(request, response);
			}

		} else {
			errorMap.put("email", "This email not exist!");
			request.setAttribute("email", email);
			request.setAttribute("errorMap", errorMap);
			doGet(request, response);
		}
	}

}
