package ua.sigma.web.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import ua.sigma.entity.User;
import ua.sigma.service.MailService;
import ua.sigma.service.UserService;
import ua.sigma.service.mail.MailMessages;
import ua.sigma.service.mail.MailSubject;
import ua.sigma.web.Path;
import ua.sigma.web.SystemMessage;
import ua.sigma.web.bean.FormRegistrationBean;
import ua.sigma.web.command.user.Validator;

public class RegistrationServlet extends HttpServlet {

	private Logger logger = Logger.getLogger(RegistrationServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher(Path.PAGE_REGISTRATION).forward(request,
				response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FormRegistrationBean formBean = new FormRegistrationBean(
				request.getParameter("firsName"),
				request.getParameter("lastName"),
				request.getParameter("email"), request.getParameter("pswd"),
				request.getParameter("cpswd"), request.getParameter("check"), request.getParameter("check1"));

		logger.debug("Form registration bean -->> " + formBean);

		Map<String, String> errorMap = new Validator()
				.validateRegistrationForm(formBean);

		logger.debug("Error map size -->> " + errorMap.size());
		if (errorMap.size() == 0) {
			UserService userService = (UserService) request.getServletContext()
					.getAttribute("userService");
			User user = convertFormBeanToUser(formBean);

			if (userService.exist(user.getEmail())) {
				errorMap.put("email", "User with this email exist");
				formBean.setConfirmPassword("");
				formBean.setPassword("");
				request.setAttribute("form", formBean);
				request.setAttribute("errorMap", errorMap);
				doGet(request, response);
			} else {
				String[] recipient = new String[] { user.getEmail() };
				new MailService().sendConfirmMessage(recipient,
						MailSubject.REGISTRATION,
						MailMessages.REGISTRATION_ON_SYSTEM);
				userService.add(user);

				request.setAttribute("messageSubject",
						SystemMessage.SUBJECT_WAIT);
				request.setAttribute("messageText", SystemMessage.MESSAGE_WAIT);

				request.getRequestDispatcher(Path.PAGE_MESSAGE).forward(
						request, response);
			}
		}

		else {
			request.setAttribute("form", formBean);
			request.setAttribute("errorMap", errorMap);
			doGet(request, response);
		}

	}

	private User convertFormBeanToUser(FormRegistrationBean formBean) {
		int roleId = Integer.valueOf(formBean.getUserRole());
		int status = Integer.valueOf(formBean.getStatus());
		return new User(formBean.getFirstName(), formBean.getLastName(),
				formBean.getEmail(), formBean.getPassword(), roleId, status);
	}

}
