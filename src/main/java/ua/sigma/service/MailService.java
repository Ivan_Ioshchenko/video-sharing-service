package ua.sigma.service;

import ua.sigma.service.mail.MailSend;

public class MailService {
	private MailSend sender = new MailSend();

	public void sendConfirmMessage(String[] recipient, String subject,
			String text) {
		sender.sendMail(recipient, subject, text);
	}

}
