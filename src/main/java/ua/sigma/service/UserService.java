package ua.sigma.service;

import org.apache.log4j.Logger;

import ua.sigma.db.DAO.UserDAO;
import ua.sigma.db.DAO.MySQL.UserDAOImpl;
import ua.sigma.entity.User;

public class UserService {
	private UserDAO userDAO;
	private Logger logger = Logger.getLogger(UserService.class);

	public UserService() {
		logger.debug("Set connection with DB");
		userDAO = new UserDAOImpl();
	}

	public boolean exist(String email) {
		User value = userDAO.get(email);
		logger.debug("User from base -->> " + value);

		return value == null ? false : true;
	}

	public void add(User user) {
		userDAO.add(user);
		logger.debug("Add user to DB -->" + user);
	}

	public User get(String email) {
		return userDAO.get(email);
	}

	public void recoveryPassword(String email, String password) {
		userDAO.update(email, password);
	}

}
