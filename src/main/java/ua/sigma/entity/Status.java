package ua.sigma.entity;

public enum Status {
	PENDING, CONFIRMED, DENIED;

	public static Role getRole(User user) {
		int status = user.getStatus();
		return Role.values()[status];
	}

	public String getName() {
		return name().toLowerCase();
	}

}
